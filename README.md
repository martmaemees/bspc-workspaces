# BSPWM Workspaces

A utility for usage with the [bspwm](https://github.com/baskerville/bspwm) window manager to add the functionality of workspaces to the window manager. A workspace in this project is an entity that wraps around multiple workspaces across all the monitors. Changing the active workspace will change the active desktops across all monitors, allowing separating the nodes into separate contexts.

## Explanation

TODO

## Installation

TODO

## Usage

To see the available commands of the application, run the following command.

```shell
$ bspws --help
```

### Example setup

TODO
