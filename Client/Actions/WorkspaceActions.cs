using Ardalis.GuardClauses;
using BspwmWorkspaces.Client.Clients;
using BspwmWorkspaces.Client.Data;

namespace BspwmWorkspaces.Client.Actions;

public static class WorkspaceActions
{
    public static async Task<WorkspaceState> ActivateWorkspace(string name, WorkspaceState state)
    {
        Guard.Against.NullOrWhiteSpace(name, nameof(name));
        Guard.Against.Null(state, nameof(state));
        
        if (!state.Workspaces.ContainsKey(name)) {
            throw new ArgumentException($"Workspace with name '{name}' does not exist.");
        }

        if (!state.Workspaces.ContainsKey(state.ActiveWorkspace)) {
            throw new Exception($"Active workspace is {state.ActiveWorkspace} but it does not exist in the state.");
        }

        if (state.ActiveWorkspace == name) {
            Console.WriteLine($"Workspace {name} is already active.");
            return state;
        }

        var activeWorkspace = state.Workspaces[state.ActiveWorkspace];
        var newWorkspace = state.Workspaces[name];

        // var activeDesktopName = await BspwmClient.GetFocusedDesktopName();

        var currentFocusedDesktopIds = await BspwmClient.GetFocusedDesktopIds();
        
        foreach (var desktopToFocus in newWorkspace.FocusedDesktops) {
            await BspwmClient.FocusDesktop(desktopToFocus);
        }

        // Sticky nodes seem to prevent certain desktops from swapping and focusing.
        // Temporarily turning off the sticky flag on them while we do the swapping. 
        // var stickyNodeIds = await BspwmClient.GetStickyNodeIds();
        // foreach (var stickyNodeId in stickyNodeIds) {
        //     await BspwmClient.ToggleStickyOnNode(stickyNodeId);
        // }

        // for (int i = 0; i < activeWorkspace.Count && i < newWorkspace.Count; i++) {
        //     var oldD = activeWorkspace[i];
        //     var newD = newWorkspace[i];
        //     await BspwmClient.RenameDesktop(oldD.Id, $"{state.ActiveWorkspace}/{oldD.Name}");
        //     await BspwmClient.RenameDesktop(newD.Id, newD.Name);
        //     await BspwmClient.SwapDesktop(oldD.Id, newD.Id);
        // }
        
        // foreach (var stickyNodeId in stickyNodeIds) {
        //     await BspwmClient.ToggleStickyOnNode(stickyNodeId);
        // }

        // await BspwmClient.FocusDesktop(activeDesktopName);

        state.Workspaces[state.ActiveWorkspace] = activeWorkspace with { FocusedDesktops = currentFocusedDesktopIds };
        return state with { ActiveWorkspace = name };
    }

    public static string GetDesktopFullName(string name, WorkspaceState state)
    {
        if (state.ActiveWorkspace == Constants.MainWorkspaceName) {
            return name;
        }

        return $"{state.ActiveWorkspace}/{name}";
    }
}
