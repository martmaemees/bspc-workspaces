using BspwmWorkspaces.Client.Actions;
using BspwmWorkspaces.Client.Repositories;
using CommandLine;

namespace BspwmWorkspaces.Client.Verbs;

[Verb("next", HelpText = "Activate next workspace")]
public class NextWorkspace : Verb
{
    protected override async Task RunAsyncImpl()
    {
        var state = await WorkspacesRepository.GetWorkspaces();

        if (state.Workspaces.Count == 1) {
            Console.WriteLine("Only one workspace exists. Cannot activate next workspace.");
            return;
        }

        string? newWorkspaceName = null;
        bool currentFound = false;
        foreach (var (name, ws) in state.Workspaces) {
            if (currentFound) {
                newWorkspaceName = name;
                break;
            }

            if (state.ActiveWorkspace == name) {
                currentFound = true;
            }

            // Set new name as the first workspace, for if the current one is the last one.
            newWorkspaceName ??= name;
        }

        var newState = await WorkspaceActions.ActivateWorkspace(newWorkspaceName!, state);
        
        await WorkspacesRepository.WriteWorkspaces(newState);
    }
}
