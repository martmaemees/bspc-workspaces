using BspwmWorkspaces.Client.Clients;
using BspwmWorkspaces.Client.Repositories;
using CommandLine;

namespace BspwmWorkspaces.Client.Verbs;

[Verb("move", HelpText = "Move active node to workspace")]
public class MoveActiveNodeToWorkspace : Verb
{
    [Value(0, HelpText = "Target workspace name.")]
    public string WorkspaceName { get; set; }

    protected override async Task RunAsyncImpl()
    {
        if (string.IsNullOrWhiteSpace(WorkspaceName)) {
            throw new ArgumentException("Target workspace name is required.");
        }
        
        var workspaces = await WorkspacesRepository.GetWorkspaces();

        var workspace = workspaces.Workspaces[WorkspaceName];
        if (workspace == null) {
            throw new ArgumentException($"Workspace with name {WorkspaceName} does not exist.");
        }

        // FIXME: Does not work with new behaviour model.
        var focusedNodeId = await BspwmClient.GetFocusedNodeId();
        var fromDesktop = await BspwmClient.GetDesktopNameOfNode(focusedNodeId);

        await BspwmClient.MoveNodeToDesktop(focusedNodeId, $"{WorkspaceName}/{fromDesktop}");
    }
}
