using BspwmWorkspaces.Client.Clients;
using CommandLine;

namespace BspwmWorkspaces.Client.Verbs;

public abstract class Verb : IVerb
{
    [Option('v', "verbose")]
    public bool Verbose { get; set; }

    protected abstract Task RunAsyncImpl();
    
    public async Task RunAsync()
    {
        if (Verbose) {
            BspwmSocket.LogCommand = true;
        }
        await RunAsyncImpl();
    }
}

public interface IVerb
{
    Task RunAsync();
}
