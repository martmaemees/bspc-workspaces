using BspwmWorkspaces.Client.Clients;
using BspwmWorkspaces.Client.Data;
using BspwmWorkspaces.Client.Repositories;
using CommandLine;

namespace BspwmWorkspaces.Client.Verbs;

[Verb("add", HelpText = "Add new workspace")]
public class AddWorkspace : Verb
{
    [Value(0, Required = true, HelpText = "Name of the new workspace.")]
    public string Name { get; set; }


    protected override async Task RunAsyncImpl()
    {
        var workspaces = await WorkspacesRepository.GetWorkspaces();
        if (workspaces.Workspaces.ContainsKey(Name)) {
            throw new ArgumentException($"Workspace with name '{Name}' already exists.");
        }

        var initialState = await GetInitialWorkspaceState(workspaces);
        List<Desktop> newDesktops = new List<Desktop>();

        ISet<int> focusedDesktopIds = new HashSet<int>();
        foreach (var (monitorId, desktopList) in initialState) {
            var desktopIds = await BspwmClient.AddDesktops(
                monitorId, desktopList.Select(d => $"{Name}/{d}").ToList()
            );
            for (var i = 0; i < desktopIds.Count && i < desktopList.Count; i++) {
                if (i == 0) {
                    focusedDesktopIds.Add(desktopIds[i]);
                }
                newDesktops.Add(new Desktop(desktopIds[i], desktopList[i]));
            }
        }
        
        workspaces.Workspaces.Add(Name, new Workspace(newDesktops, focusedDesktopIds));
        await WorkspacesRepository.WriteWorkspaces(workspaces);
    }

    private static async Task<IDictionary<int, List<string>>> GetInitialWorkspaceState(WorkspaceState state)
    {
        var main = state.Workspaces[Constants.MainWorkspaceName];
        IDictionary<int, List<string>> result = new Dictionary<int, List<string>>();

        foreach (var (id, name) in main.Desktops) {
            var monitorId = await BspwmClient.GetMonitorIdOfDesktop(id);
            if (!result.ContainsKey(monitorId)) {
                result.Add(monitorId, new List<string>());
            }
            result[monitorId].Add(name);
        }

        return result;
    }
}
