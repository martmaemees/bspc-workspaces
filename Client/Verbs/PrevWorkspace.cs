using BspwmWorkspaces.Client.Actions;
using BspwmWorkspaces.Client.Repositories;
using CommandLine;

namespace BspwmWorkspaces.Client.Verbs;

[Verb("prev", false,new []{"previous"}, HelpText = "Activate previous workspace")]
public class PrevWorkspace : Verb
{
    protected override async Task RunAsyncImpl()
    {
        var state = await WorkspacesRepository.GetWorkspaces();

        if (state.Workspaces.Count == 1) {
            Console.WriteLine("Only one workspace exists. Cannot activate previous workspace.");
            return;
        }
        
        string? newWorkspaceName = null;
        foreach (var (name, ws) in state.Workspaces) {
            if (state.ActiveWorkspace == name && newWorkspaceName is not null) {
                break;
            }
            
            newWorkspaceName = name;
        }

        var newState = await WorkspaceActions.ActivateWorkspace(newWorkspaceName!, state);
        
        await WorkspacesRepository.WriteWorkspaces(newState);
    }
}
