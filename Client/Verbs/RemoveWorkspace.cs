using BspwmWorkspaces.Client.Clients;
using BspwmWorkspaces.Client.Repositories;
using CommandLine;

namespace BspwmWorkspaces.Client.Verbs;

[Verb("remove", HelpText = "Add new workspace")]
public class RemoveWorkspace : Verb
{
    [Value(0, Required = true, HelpText = "Name of the workspace to remove.")]
    public string Name { get; set; }

    protected override async Task RunAsyncImpl()
    {
        if (Name == Constants.MainWorkspaceName) {
            Console.WriteLine($"Cannot remove the {Constants.MainWorkspaceName} workspace as of now."
                              + $" The tool will break if you were to do so. Aborting.");
            return;
        }
        
        var workspaces = await WorkspacesRepository.GetWorkspaces();
        if (!workspaces.Workspaces.ContainsKey(Name)) {
            throw new ArgumentException($"Workspace with name '{Name}' does not exist.");
        }

        var workspace = workspaces.Workspaces[Name];
        foreach (var (desktopId, name) in workspace.Desktops) {
            Console.WriteLine($"Removing desktop {name} (0x{desktopId:x8}) of workspace {Name}");
            await BspwmClient.RemoveDesktop(desktopId);
        }

        workspaces.Workspaces.Remove(Name);
        await WorkspacesRepository.WriteWorkspaces(workspaces);
    }
}
