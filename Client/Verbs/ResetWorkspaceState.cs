using BspwmWorkspaces.Client.Repositories;
using CommandLine;

namespace BspwmWorkspaces.Client.Verbs;

[Verb("reset", HelpText = "Reset state making all current workspaces part of the Main workspace.")]
public class ResetWorkspaceState : Verb
{
    protected override async Task RunAsyncImpl()
    {
        await WorkspacesRepository.ResetState();
    }
}
