using BspwmWorkspaces.Client.Actions;
using BspwmWorkspaces.Client.Clients;
using BspwmWorkspaces.Client.Repositories;
using CommandLine;

namespace BspwmWorkspaces.Client.Verbs;

[Verb("move-node", HelpText = "Move current node to a desktop in the current workspace.")]
public class MoveNodeToDesktop : Verb
{
    [Value(0, Required = true, HelpText = "Name of the desktop without the workspace prefix.")]
    public string Name { get; set; }

    protected override async Task RunAsyncImpl()
    {
        var state = await WorkspacesRepository.GetWorkspaces();

        await BspwmClient.MoveCurrentNodeToDesktop(WorkspaceActions.GetDesktopFullName(Name, state));
    }
}
