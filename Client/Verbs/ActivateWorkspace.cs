using BspwmWorkspaces.Client.Actions;
using BspwmWorkspaces.Client.Repositories;
using CommandLine;

namespace BspwmWorkspaces.Client.Verbs;

[Verb("activate", HelpText = "Activate another workspace")]
public class ActivateWorkspace : Verb
{
    [Value(0, Required = true, HelpText = "Name of the workspace to be activated.")]
    public string Name { get; set; }

    protected override async Task RunAsyncImpl()
    {
        var state = await WorkspacesRepository.GetWorkspaces();

        var newState = await WorkspaceActions.ActivateWorkspace(Name, state);
        
        await WorkspacesRepository.WriteWorkspaces(newState);
    }
}
