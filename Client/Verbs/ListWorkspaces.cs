using BspwmWorkspaces.Client.Repositories;
using CommandLine;

namespace BspwmWorkspaces.Client.Verbs;

[Verb("list", HelpText = "List workspaces")]
public class ListWorkspaces : Verb
{
    [Option("ids", Required = false, Default = false, HelpText = "Write ids of desktops instead of names.")]
    public bool Ids { get; set; }
    
    [Option("exclude-active", Required = false, Default = false, 
        HelpText = "Exclude the active workspace from results.")]
    public bool ExcludeActive { get; set; }

    protected override async Task RunAsyncImpl()
    {
        var root = await WorkspacesRepository.GetWorkspaces();

        foreach (var (name, workspace) in root.Workspaces) {
            if (ExcludeActive && name == root.ActiveWorkspace) {
                continue;
            }
            Console.WriteLine(
                // $"{name}: {string.Join(", ", workspace.Desktops.Select(d => Ids ? $"{d.Id:x8}" : d.Name).ToList())}"
                name
            );
        }
    }
}
