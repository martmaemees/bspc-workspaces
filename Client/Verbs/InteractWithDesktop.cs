using BspwmWorkspaces.Client.Actions;
using BspwmWorkspaces.Client.Clients;
using BspwmWorkspaces.Client.Repositories;
using CommandLine;

namespace BspwmWorkspaces.Client.Verbs;

[Verb("interact-desktop", HelpText = "Send a command to interact with a desktop in the active workspace.")]
public class InteractWithDesktop : Verb
{
    [Value(0, Required = true, HelpText = "Name of the desktop without the workspace prefix.")]
    public string Name { get; set; }
    
    [Value(1, Required = true, 
        HelpText = "Command to be executed on the desktop without the '--' prefix (e.g. activate, to-monitor, etc.).")]
    public string Command { get; set; }
    
    [Value(2, Required = false, HelpText = "Arguments to the command (e.g. monitor name for --to-monitor).")]
    public IEnumerable<string> Arguments { get; set; }

    protected override async Task RunAsyncImpl()
    {
        var state = await WorkspacesRepository.GetWorkspaces();

        var res = await BspwmClient.DesktopCommand(WorkspaceActions.GetDesktopFullName(Name, state), Command, 
            Arguments.ToList());
        if (res.Length > 0) {
            Console.WriteLine(res);
        }
    }
}
