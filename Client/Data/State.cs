namespace BspwmWorkspaces.Client.Data;

/*
 * Bspwm state.
 */
public record Root(Dictionary<int, Monitor> Monitors, Monitor ActiveMonitor);

public record Monitor(int Id, string Name, List<Desktop> Desktops, int FocusedDesktopId);

public record Desktop(int Id, string Name);

/*
 * Custom state.
 */
public record Workspace(IList<Desktop> Desktops, ISet<int> FocusedDesktops);

public record WorkspaceState(IDictionary<string, Workspace> Workspaces, string ActiveWorkspace);
