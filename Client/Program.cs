﻿// See https://aka.ms/new-console-template for more information

using BspwmWorkspaces.Client.Verbs;
using CommandLine;

// Console.WriteLine(string.Join(", ", await BspwmClient.GetAllDesktopIds()));

var mainParser = new Parser(config => config.HelpWriter = Console.Out);

Type[] verbs = new[]
{
    typeof(ListWorkspaces),
    typeof(AddWorkspace),
    typeof(RemoveWorkspace),
    typeof(ActivateWorkspace),
    typeof(NextWorkspace),
    typeof(PrevWorkspace),
    typeof(ResetWorkspaceState),
    typeof(MoveActiveNodeToWorkspace),
    typeof(InteractWithDesktop),
    typeof(MoveNodeToDesktop)
};

await mainParser.ParseArguments(args, verbs)
    .WithParsedAsync<IVerb>(async (verb) => { await verb.RunAsync(); });
