using System.Text.Json;
using BspwmWorkspaces.Client.Clients;
using BspwmWorkspaces.Client.Data;

namespace BspwmWorkspaces.Client.Repositories;

public static class WorkspacesRepository
{
    private const string WorkspaceFile = "bspwm-workspaces/workspaces.json";
    private static readonly JsonSerializerOptions _jsonOptions = new();

    static WorkspacesRepository()
    {
        _jsonOptions.WriteIndented = true;
    }

    public static async Task<WorkspaceState> GetWorkspaces()
    {
        var root = await BspwmClient.GetBspwmState();

        Dictionary<int, string> desktops = new Dictionary<int, string>();
        foreach (var (_, (_, _, monitorDesktops, _)) in root.Monitors) {
            foreach (var (id, name) in monitorDesktops) {
                desktops[id] = name;
            }
        }

        var state = await ReadWorkspaces();
        if (state == null) {
            return GetDefaultState(root);
        }

        bool activeDeleted = false;
        foreach (var (name, workspace) in state.Workspaces) {
            var filteredDesktops = workspace.Desktops
                .Where(d =>
                {
                    var res = desktops.ContainsKey(d.Id)
                              && (desktops[d.Id] == d.Name || desktops[d.Id] == $"{name}/{d.Name}");
                    if (res) {
                        desktops.Remove(d.Id);
                    } else {
                        workspace.FocusedDesktops.Remove(d.Id);
                    }

                    return res;
                })
                .ToList();
            if (filteredDesktops.Count > 0) {
                state.Workspaces[name] = workspace with { Desktops = filteredDesktops };
            } else {
                state.Workspaces.Remove(name);
                if (name == Constants.MainWorkspaceName) {
                    return GetDefaultState(root);
                }

                if (name == state.ActiveWorkspace) {
                    activeDeleted = true;
                }
            }
        }

        if (desktops.Count > 0) {
            Console.WriteLine("Not all desktops were stored in the workspaces state file. Resetting state.");
            return GetDefaultState(root);
        }

        if (activeDeleted) {
            return state with { ActiveWorkspace = Constants.MainWorkspaceName };
        }

        return state;
    }

    private static async Task<WorkspaceState?> ReadWorkspaces()
    {
        if (!File.Exists(GetWorkspacesFilePath())) {
            return null;
        }

        await using FileStream openStream = File.OpenRead(GetWorkspacesFilePath());
        return await JsonSerializer.DeserializeAsync<WorkspaceState>(openStream, _jsonOptions);
    }

    public static async Task WriteWorkspaces(WorkspaceState state)
    {
        var filePath = GetWorkspacesFilePath();
        var dirPath = Path.GetDirectoryName(filePath);
        if (!Directory.Exists(dirPath)) {
            Console.WriteLine($"Creating directory {dirPath}");
            Directory.CreateDirectory(dirPath);
        }

        await using FileStream writeStream = File.Create(GetWorkspacesFilePath());
        await JsonSerializer.SerializeAsync(writeStream, state, _jsonOptions);
    }

    public static async Task ResetState()
    {
        var root = await BspwmClient.GetBspwmState();
        await WriteWorkspaces(GetDefaultState(root));
    }

    private static WorkspaceState GetDefaultState(Root root)
    {
        return new WorkspaceState(
            new Dictionary<string, Workspace>()
            {
                {
                    "Main", new Workspace(
                        root.Monitors.Values.SelectMany(m => m.Desktops).ToList(),
                        root.Monitors.Values.Select(m => m.FocusedDesktopId).ToHashSet()
                    )
                }
            },
            "Main"
        );
    }

    private static string GetWorkspacesFilePath()
    {
        var appData = Environment.GetFolderPath(
            Environment.SpecialFolder.ApplicationData,
            Environment.SpecialFolderOption.Create
        );
        return Path.Join(appData, WorkspaceFile);
    }
}
