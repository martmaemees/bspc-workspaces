using System.Globalization;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using BspwmWorkspaces.Client.Data;
using Monitor = BspwmWorkspaces.Client.Data.Monitor;

namespace BspwmWorkspaces.Client.Clients;

public static class BspwmClient
{
    private static JsonSerializerOptions _jsonOptions = new();

    static BspwmClient()
    {
        _jsonOptions.PropertyNameCaseInsensitive = true;
    }

    public static async Task<List<string>> GetAllDesktopNames()
    {
        var result = await BspwmSocket.SendCommand("query -D --names");
        return result.Split('\n').ToList();
    }

    public static async Task<List<int>> GetAllDesktopIds()
    {
        return (await BspwmSocket.SendCommand("query -D"))
            .Trim()
            .Split('\n')
            .Select(id => int.Parse(id.Trim().Substring(2), NumberStyles.HexNumber))
            .ToList();
    }

    public static async Task<int> GetMonitorIdOfDesktop(int desktopId)
    {
        return int.Parse((await BspwmSocket.SendCommand($"query -M -d 0x{desktopId:x8}"))
            .Trim().Substring(2), NumberStyles.HexNumber);
    }

    public static async Task<List<int>> AddDesktops(int monitorId, List<string> desktopNames)
    {
        var addRes = await BspwmSocket.SendCommand(
            $"monitor 0x{monitorId:x8} --add-desktops {string.Join(' ', desktopNames)}");

        if (addRes.Length > 0) {
            Console.WriteLine($"Add result: {addRes}");
        }

        List<int> ids = new List<int>();
        foreach (var desktopName in desktopNames) {
            var idRes = await BspwmSocket.SendCommand($"query -D -d {desktopName.Trim()}");
            ids.Add(int.Parse(idRes.Trim().Substring(2), NumberStyles.HexNumber));
        }

        return ids;
    }

    public static async Task RemoveDesktop(int desktopId)
    {
        var res = await BspwmSocket.SendCommand($"desktop 0x{desktopId:x8} --remove");
        if (res.Length > 0) {
            Console.WriteLine($"Remove result: {res}");
        }
    }

    public static async Task RenameDesktop(int desktopId, string newName)
    {
        var res = await BspwmSocket.SendCommand($"desktop 0x{desktopId:x8} --rename {newName.Trim()}");
        if (res.Length > 0) {
            Console.WriteLine($"Rename result: {res}");
        }
    }

    public static async Task FocusDesktop(int desktopId)
    {
        var res = await BspwmSocket.SendCommand($"desktop 0x{desktopId:x8} --focus");
        if (res.Length > 0) {
            Console.WriteLine($"Focus result {res}");
        }
    }

    public static async Task FocusDesktop(string desktopName)
    {
        var res = await BspwmSocket.SendCommand($"desktop {desktopName} --focus");
        if (res.Length > 0) {
            Console.WriteLine($"Focus result {res}");
        }
    }

    public static async Task SwapDesktop(int targetId, int withId)
    {
        var res = await BspwmSocket.SendCommand($"desktop 0x{targetId:x8} -s 0x{withId:x8}");
        if (res.Length > 0) {
            Console.WriteLine($"Swap result {res}");
        }
    }

    public static async Task<HashSet<string>> GetFocusedDesktopNames()
    {
        var monitorIds = (await BspwmSocket.SendCommand("query -M")).Trim().Split('\n')
            .Select(s => s.Trim());

        HashSet<string> res = new HashSet<string>();
        foreach (var monitorId in monitorIds) {
            var desktopName = (await BspwmSocket.SendCommand($"query -D -d {monitorId}:focused --names"))
                .Trim();
            res.Add(desktopName);
        }

        return res;
    }

    public static async Task<string> GetFocusedDesktopName()
    {
        return (await BspwmSocket.SendCommand("query -D -d focused --names")).Trim();
    }

    public static async Task<HashSet<int>> GetFocusedDesktopIds()
    {
        var monitorIds = (await BspwmSocket.SendCommand("query -M")).Trim().Split('\n')
            .Select(s => s.Trim());

        HashSet<int> res = new HashSet<int>();
        foreach (var monitorId in monitorIds) {
            var desktopId = (await BspwmSocket.SendCommand($"query -D -d {monitorId}:focused"))
                .Trim();
            res.Add(int.Parse(desktopId.Substring(2), NumberStyles.HexNumber));
        }

        return res;
    }

    public static async Task<int> GetFocusedDesktopId()
    {
        var res = (await BspwmSocket.SendCommand("query -D -d focused")).Trim();
        return int.Parse(res.Substring(2), NumberStyles.HexNumber);
    }

    public static async Task<List<int>> GetStickyNodeIds()
    {
        var resStr = (await BspwmSocket.SendCommand("query -N -n .sticky")).Trim().Split('\n');
        return resStr.Select(str => int.Parse(str.Substring(2), NumberStyles.HexNumber)).ToList();
    }

    public static async Task ToggleStickyOnNode(int nodeId)
    {
        var res = await BspwmSocket.SendCommand($"node 0x{nodeId:x8} --flag sticky");
        if (res.Length > 0) {
            Console.WriteLine($"Sticky result: {res}");
        }
    }

    public static async Task<int> GetFocusedNodeId()
    {
        var res = (await BspwmSocket.SendCommand("query -N -n focused")).Trim();
        return int.Parse(res.Substring(2), NumberStyles.HexNumber);
    }

    public static async Task<string> GetDesktopNameOfNode(int nodeId)
    {
        return (await BspwmSocket.SendCommand($"query -D -n 0x{nodeId:x8} --names")).Trim();
    }

    public static async Task MoveNodeToDesktop(int nodeId, string desktopName)
    {
        var res = (await BspwmSocket.SendCommand($"node 0x{nodeId:x8} --to-desktop {desktopName}")).Trim();
        if (res.Length > 0) {
            Console.WriteLine($"Move result: {res}");
        }
    }

    public static async Task MoveCurrentNodeToDesktop(string desktopName)
    {
        var res = (await BspwmSocket.SendCommand($"node --to-desktop {desktopName}")).Trim();
        if (res.Length > 0) {
            Console.WriteLine($"Move result: {res}");
        }
    }

    public static async Task ActivateDesktop(string desktopName)
    {
        var res = (await BspwmSocket.SendCommand($"desktop {desktopName} --activate")).Trim();
        if (res.Length > 0) {
            Console.Write($"ActivateDesktop result: {res}");
        }
    }

    public static async Task<string> DesktopCommand(string desktopName, string command, List<string> arguments)
    {
        return (await BspwmSocket.SendCommand($"desktop {desktopName} --{command} {string.Join(" ", arguments)}")).Trim();
    }

    public static async Task<Root> GetBspwmState()
    {
        var monitorListString = await BspwmSocket.SendCommand("query -M");
        var monitorIds = monitorListString.Trim().Split('\n').Select(s => s.Trim());
        var activeMonitorId = (await BspwmSocket.SendCommand("query -M -m")).Trim();

        Dictionary<int, Monitor> monitors = new Dictionary<int, Monitor>();
        Monitor? activeMonitor = null;

        foreach (var monitorId in monitorIds) {
            Monitor? monitor;
            using (var jsonStream = await BspwmSocket.SendCommandStream($"query -T -m {monitorId}")) {
                monitor = await JsonSerializer.DeserializeAsync<Monitor>(jsonStream, _jsonOptions);
            }

            if (monitor == null) {
                throw new Exception("Monitor query deserialized to null!");
            }

            // Console.WriteLine($"Monitor: {monitor}");
            monitors.Add(monitor.Id, monitor);
            if (monitorId == activeMonitorId) {
                activeMonitor = monitor;
            }
        }

        if (activeMonitor is null) {
            throw new Exception("No active monitor found!");
        }

        return new Root(monitors, activeMonitor);
    }
}

static class BspwmSocket
{
    private const string BspwmSocketPath = "/tmp/bspwm_0_0-socket";

    public static bool LogCommand = false;

    public static async Task<string> SendCommand(string command)
    {
        if (LogCommand) {
            Console.WriteLine($"Command: {command}");
        }
        using var socket = ConnectSocket();
        await using var stream = new NetworkStream(socket, FileAccess.ReadWrite, true);
        var requestBytes = BuildRequestBytes(command);
        await stream.WriteAsync(requestBytes);

        StreamReader reader = new StreamReader(stream);
        var responseString = await reader.ReadToEndAsync();

        return responseString;
    }

    public static async Task<Stream> SendCommandStream(string command)
    {
        var socket = ConnectSocket();
        var stream = new NetworkStream(socket, FileAccess.ReadWrite, true);

        var requestBytes = BuildRequestBytes(command);
        await stream.WriteAsync(requestBytes);

        return stream;
    }

    private static byte[] BuildRequestBytes(string command)
    {
        string requestString = command.Replace(' ', '\x00');
        if (!requestString.EndsWith('\x00')) {
            requestString += '\x00';
        }

        return Encoding.UTF8.GetBytes(requestString);
    }

    private static Socket ConnectSocket()
    {
        var socket = new Socket(AddressFamily.Unix, SocketType.Stream, ProtocolType.IP);
        socket.Connect(new UnixDomainSocketEndPoint(BspwmSocketPath));
        return socket;
    }
}
